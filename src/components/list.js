import React from 'react';
import Tile from './tile';
import styled from 'styled-components';

const ListStyle = styled.ul`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
    list-style: none;
    padding-right: 20px;
`;

const List = (props) => {
    const arr = props.arr;
    const listItems = arr.map((item) => <Tile data={item} key={item.name}></Tile>);

    return (
        <ListStyle>{listItems}</ListStyle>
    );
};

export default List;