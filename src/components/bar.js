import React, {useState} from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Actions from '../redux/actions.js';

const BarStyle = styled.div`
    background-color: #16ab8b;
    width: 100%;
    text-align: center;
    margin-top: 50px;
    padding: 10px 0;
    min-height: 100px;

    .form-item {
        margin: 0 auto;
        width: 400px;
        text-align: left;
        margin-bottom: 20px;

        span {
            display: inline-block;
            width: 150px;
        }

        input {
            width: 200px;
        }
    }

    button {
        background-color: #3a7de8;
        color: #FFF;
        cursor: pointer;
        height: 30px;
        line-height: 30px;
        font-size: 14px;
        width: 120px;
        border: 0;
        outline: none;

        &:hover {
            font-size: 16px;
        }
    }
`;

const Bar = (props) => {
    let [city, setCity] = useState('');
    const [interval, setInterval] = useState(1000);
    const { addCityWeatheritem, cityWeatherList } = props;

    const handleCityChange = (e) => {
        setCity(e.target.value);
    };

    const handleIntervalChange = (e) => {
        setInterval(e.target.value);
    };

    const addItem = () => {
        if (!city) {
            return;
        }

        if (cityWeatherList.length >= 5) {
            alert('At most 5 cities!');
            return;
        }

        if (cityWeatherList.findIndex(item => item.name.toLowerCase() === city) !== -1) {
            setCity('');
            alert('City already exist!');
            return;
        }

        addCityWeatheritem(city, interval);
        setCity('');
    };

    return (
        <BarStyle>
            <div className="form-item">
                <span>Enter city: </span>
                <input type="text" value={city} onChange={handleCityChange} />
            </div>

            <div className="form-item">
                <span>Pick an interval:</span>

                <select value={interval} onChange={handleIntervalChange}>
                    <option value="1000">1 secs</option>
                    <option value="5000">5 secs</option>
                    <option value="10000">10 secs</option>
                    <option value="15000">25 secs</option>
                </select>
            </div>

            <button className="add" onClick={addItem}>Add</button>
        </BarStyle>
    );
};

const mapStateToProps = (state) => {
    return {
        cityWeatherList: state.cityWeatherList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addCityWeatheritem (name, interval) {
            dispatch(Actions.addCityWeatheritem(name, interval));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Bar);