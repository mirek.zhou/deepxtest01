import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Actions from '../redux/actions.js';
import Utils from '../utils/useInterval';

const TileStyle = styled.li`
    flex-grow: 1;
    min-width: 300px;
    height: 200px;
    line-height: 200px;
    background-color: yellow;
    text-align: center;
    position: relative;
    margin-top: 20px;
    margin-left: 20px;

    .city-name {
        line-height: 20px;
        position: absolute;
        left: 10px;
        top: 10px;
        font-size: 22px;
        font-weight: bold;
    }

    .interval {
        line-height: 20px;
        position: absolute;
        left: 10px;
        top: 40px;
        font-size: 16px;
    }

    .temperature {
        line-height: 20px;
        position: absolute;
        left: 10px;
        bottom: 10px;
        font-size: 18px;
        font-weight: bold;
    }

    .description {
        line-height: 20px;
        position: absolute;
        right: 10px;
        top: 10px;
        font-size: 18px;
        font-weight: bold;
    }

    img {
        position: absolute;
        right: 0;
        top: 20px;
    }

    button {
        background-color: #3a7de8;
        color: #FFF;
        cursor: pointer;
        position: absolute;
        right: 10px;
        bottom: 5px;
        height: 30px;
        line-height: 30px;
        font-size: 14px;
        width: 80px;
        border: 0;
        outline: none;

        &:hover {
            font-size: 16px;
        }
    }

    .timer {
        font-size: 30px;
        font-weight: bold;
        width: 100px;
        height: 60px;
        line-height: 60px;
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -50px;
        margin-top: -30px;
    }
`;

const Tile = (props) => {
    const { removeCityWeatheritem } = props;
    const [count, setCount] = useState(0);
    const interval = props.data.interval ? props.data.interval : 2000;

    Utils.useInterval(() => {
        setCount(count + 1);
    }, interval);
 
    const removeItem = () => {
        console.log(`Item ${props.data.name} removed!`);
        removeCityWeatheritem(props.data.name);
    };

    return (
        <TileStyle>
            <span className="city-name">{props.data.name}</span>
            <span className="temperature">{props.data.main.temp} ℃</span>
            <span className="description">{props.data.weather[0].description}</span>
            <img alt={props.data.weather[0].description} src={`http://openweathermap.org/img/wn/${props.data.weather[0].icon}@2x.png`} />
            <button className="remove" onClick={removeItem}>Remove</button>
            <div className="timer">{count}</div>
            <div className="interval">(interval: {interval/1000}s)</div>
        </TileStyle>
    );
};

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeCityWeatheritem (name) {
            dispatch(Actions.removeCityWeatheritem(name));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tile);