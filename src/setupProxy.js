const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        proxy("/data", {
            target: "https://api.openweathermap.org/",
            changeOrigin: true,

            onProxyReq(proxyReq) {
                if (proxyReq.getHeader("origin")) {
                    proxyReq.setHeader("origin", "api.openweathermap.org/")
                }
            },
            
            logLevel: "debug",
        })
    );
};
