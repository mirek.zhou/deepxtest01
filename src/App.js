import React, { useEffect } from 'react';
import styled from 'styled-components';
import List from './components/list.js';
import Bar from './components/bar.js';
import { connect } from 'react-redux';
import Actions from './redux/actions.js';

const AppStyle = styled.div`
    padding-top: 20px
`;

const App = (props) => {
    const { cityWeatherList, getCityWeatherList } = props;

    useEffect(() => {
        getCityWeatherList();
    }, [getCityWeatherList]);

    return (
        <AppStyle>
            <List arr={cityWeatherList}></List>
            <Bar></Bar>
        </AppStyle>
    );
};

const mapStateToProps = (state) => {
    return {
        cityWeatherList: state.cityWeatherList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCityWeatherList () {
            dispatch(Actions.getCityWeatherList());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);