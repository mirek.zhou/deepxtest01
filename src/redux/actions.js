import axios from 'axios';

const getCityWeatherList = () => {
    return async (dispatch, getState) => {
        let pre = window.location.href.indexOf('localhost') === -1 ? 'https://api.openweathermap.org' : '';
        let res = await axios.get(`${pre}/data/2.5/group?id=524901,703448,2643743&units=metric&APPID=caa37aa11f7880b5d70a867da9af9ff7`);
        dispatch({ type: 'SET_CITY_WEATHER_LIST', data: res.data.list });
    };
};

const removeCityWeatheritem = (name) => {
    return (dispatch, getState) => {
        let arr = getState().cityWeatherList;
        arr.splice(arr.findIndex(item => item.name === name), 1);
        dispatch({ type: 'SET_CITY_WEATHER_LIST', data: [].concat(arr) });
        //dispatch({ type: 'SET_CITY_WEATHER_LIST', data: arr }); //why this not working?
    };
};

const addCityWeatheritem = (name, interval) => {
    return async (dispatch, getState) => {
        let res;
        let arr = getState().cityWeatherList;
        let pre = window.location.href.indexOf('localhost') === -1 ? 'https://api.openweathermap.org' : '';

        try {
            res = await axios.get(`${pre}/data/2.5/weather?q=${name}&APPID=caa37aa11f7880b5d70a867da9af9ff7`);
        } catch (err) {
            alert('City not exist!');
            return;
        }
        
        if (res.data.cod === '404') {
            alert(`${name} is not a city!`);
            return;
        }

        res.data.interval = interval;
        dispatch({ type: 'SET_CITY_WEATHER_LIST', data: arr.concat(res.data) });
    };
};

export default {
    getCityWeatherList,
    removeCityWeatheritem,
    addCityWeatheritem
};