import initState from './state.js';

const reducer = (state = initState, action) => {
    switch (action.type) {
        case "SET_CITY_WEATHER_LIST":
            return {
                ...state,
                cityWeatherList: action.data
            };
        default:
            return state;
    }
};

export default reducer;